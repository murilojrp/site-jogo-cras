const baseController = require('../controllers/baseController');

module.exports = (app) => {
    app.get('/base', baseController.getAll)
    app.get('/base/:id', baseController.getById)
    app.post('/base', baseController.post)
    app.delete('/base/:id', baseController.delete)
    app.patch ('/base', baseController.patch)
} 